#include <iostream>
using namespace std;

bool soln(string s, string str) {
  for (int i = 0; i < str.length(); i++) {
    int k = i;
    for (int j = 0; j < s.length(); j++) {
      if (k > str.length())
        break;
      if (s[j] == str[k]) {
        k = k + 1;
        if (j == s.length() - 1)
          return true;
        continue;
      } else
        break;
      return true;
    }
    k = i;
  }
  return false;
}

int main() {
  int t;
  cin >> t;
  string str = "";
  for (int i = 0; i < 18; i++) {
    str = str + "Yes";
  }
  while (t--) {
    string s;
    cin >> s;
    cout << (soln(s, str) ? "YES" : "NO") << endl;
  }
  return 0;
}
