#include <iostream>
#include <unordered_set>

using namespace std;

int main() {
    string a;
    cin>>a;
    unordered_set <char> check;
    for (auto character: a) check.insert(character);
    if (check.size() % 2 == 0) cout << "CHAT WITH HER!" << endl;
    else cout << "IGNORE HIM!" << endl;
    return 0;
}
