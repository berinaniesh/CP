#include<iostream>
using namespace std;

string soln(string s) {
    int count = 0;
    for(int i=0; i<s.size(); i++) {
        if (s[i] == '0') {
            count++;
            if (count==7) return "YES";
        } else {
            count = 0;
        }
    }
    count = 0;
    for (int i=0; i<s.size(); i++) {
        if (s[i] == '1') {
            count++;
            if (count == 7) return "YES";
        } else {
            count = 0;
        }
    }
    return "NO";
}

int main() {
    string s1;
    cin>>s1;
    cout << soln(s1)<<endl;
}
