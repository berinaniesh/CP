#include <iostream>
#include <string>
#include <vector>

using namespace std;

bool find(string s) {
  string p1 = "SW";
  string p2 = "WS";
  if (s.find(p1) != string::npos) {
    return true;
  }
  if (s.find(p2) != string::npos) {
    return true;
  }
  return false;
}

bool find_vert(vector<vector<char>> arr, int r, int c) {
  for (int i = 0; i < c; i++) {
    for (int j = 0; j < r - 1; j++) {
      char c = arr[j][i];
      char d = arr[j + 1][i];
      if ((c == 'S' && d == 'W') || (c == 'W' && d == 'S')) {
        return true;
      }
    }
  }
  return false;
}

int main() {
  int r, c;
  cin >> r >> c;
  vector<vector<char>> arr;
  for (int i = 0; i < r; i++) {
    vector<char> an;
    string s;
    cin >> s;
    if (find(s)) {
      cout << "NO" << endl;
      return 0;
    }
    for (int j = 0; j < c; j++) {
      an.push_back(s[j]);
    }
    arr.push_back(an);
  }
  if (find_vert(arr, r, c)) {
    cout << "NO" << endl;
    return 0;
  }
  cout << "YES" << endl;
  for (int i = 0; i < r; i++) {
    for (int j = 0; j < c; j++) {
      if (arr[i][j] == '.') {
        cout << 'D';
      } else
        cout << arr[i][j];
    }
    cout << endl;
  }
}
