#include <iostream>
#include <vector>

using namespace std;

int main() {
  int t;
  cin >> t;
  while (t--) {
    int size;
    cin >> size;
    int a[size];
    for (int v = 0; v < size; v++) {
      int b;
      cin >> b;
      a[v] = b;
    }
    int m = 0;
    int n = 0;
    for (auto i : a) {
      if (i > m) {
        n = m;
        m = i;
      } else if (i > n) {
        n = i;
      }
    }

    vector<int> soln;
    for (auto i : a) {
      if (i == m) {
        soln.push_back(i - n);
      } else
        soln.push_back(i - m);
    }
    for (auto i: soln) {
      cout << i << " ";
    }
    cout << endl;
  }
}
