// Create your own template by modifying this file!
#include <bits/stdc++.h>
using namespace std;

#ifdef DEBUG
     #define debug(args...)            {dbg,args; cerr<<endl;}
#else
    #define debug(args...)              // Just strip off all debug tokens
#endif

struct debugger
{
    template<typename T> debugger& operator , (const T& v)
    {    
        cerr<<v<<" ";    
        return *this;    
    }
} dbg;

int main() 
{
    int n;
    cin>>n;
    int ans = 0;
    vector<string> ops;
    while(n--) {
        string input;
        cin>>input;
        ops.push_back(input);
    }
    for (auto a: ops){
        if (a=="++X" || a == "X++") ans++;
        else ans--;
    }
    cout<<ans<<endl;
}

