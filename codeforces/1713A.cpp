#include<iostream>
#include<vector>

using namespace std;

int main () {
    int n;
    cin >> n;
    vector<int> ans_vector;
    while (n--) {
        int k;
        cin >> k;
        int arr[k][2];
        for(int i=0; i<k; i++) {
            cin >> arr[i][0] >> arr[i][1];
        }
        int ans = 0;
        int x_max = 0;
        int x_min = 0;
        int y_max = 0; 
        int y_min = 0;


        for (int i=0; i<k; i++) {
            if (arr[i][0] < x_min) x_min = arr[i][0];
            if (arr[i][0] > x_max) x_max = arr[i][0];
            if (arr[i][1] < y_min) y_min = arr[i][1];
            if (arr[i][1] > y_max) y_max = arr[i][1];
        }

        ans = 2*(x_max + y_max - x_min - y_min);

        ans_vector.push_back(ans);
    }

    for (auto a: ans_vector) {
        cout << a << endl;
    }
    return 0;
}