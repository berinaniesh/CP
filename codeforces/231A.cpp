// Create your own template by modifying this file!
#include <bits/stdc++.h>
using namespace std;

#ifdef DEBUG
     #define debug(args...)            {dbg,args; cerr<<endl;}
#else
    #define debug(args...)              // Just strip off all debug tokens
#endif

struct debugger
{
    template<typename T> debugger& operator , (const T& v)
    {    
        cerr<<v<<" ";    
        return *this;    
    }
} dbg;

int main() 
{
    int n;
    cin>>n;
    int arr[n][3];
    for(int i=0; i<n; i++){
        cin>>arr[i][0]>>arr[i][1]>>arr[i][2];
    }
    int ans=0;
    for(int i=0; i<n; i++) {
        if(arr[i][0]+arr[i][1]+arr[i][2]>1) {
            ans++;
        }
    }
    cout<<ans<<endl;
}

