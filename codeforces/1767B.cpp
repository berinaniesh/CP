#include<iostream>
#include <vector>
#include<algorithm>

using namespace std;

void solve(){
  int n;
  cin >> n;
  int size = n;
  vector<int> arr;
  while(n--) {
    int tmp;
    cin >> tmp;
    arr.push_back(tmp);
  }

  int l = arr[0];
  arr.erase(arr.begin()+0);
  sort(arr.begin(), arr.end());
  arr.insert(arr.begin(), l);
  
  int flag = 1;
  while(flag==1) {
  int swapped = 0;
  for(int i=1; i<size; i++) {
    if(arr[0] < arr[i]) {
      int tmp = arr[0] + arr[i];
      arr[0] = tmp%2 == 0?tmp/2:tmp/2+1;
      arr[i] = tmp - arr[0];
      swapped = 1;
    } 
  }
  if (swapped == 0) flag = 0;
  swapped = 0;
  }
  cout << arr[0] << endl;
}

int main() {
  int t;
  cin >> t;
  while(t--) solve();
}
