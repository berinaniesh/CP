#include <iostream>
#include <string>
#include <vector>
using namespace std;

bool is_round(int n) {
  string s = to_string(n);
  int count = 0;
  for (int i=0; i<s.length(); i++) {
    if (s[i] != '0') {
      count = count + 1;
    }
    if (count > 1) {
      return false;
    }
  }
  return true;
}

int main() {
  int t;
  cin >> t;
  vector<int> nums;
  for(int i=1; i<999999; i++) {
    if(is_round(i)) {
      nums.push_back(i);
    }
  }
  while(t--) {
    int k;
    cin >> k;
    int count = 0;
    for(auto a: nums) {
      if (a <= k) {
	count = count +1;
      } else {
	break;
      }
   }
    cout << count << endl;
  }
  return 0;
}
