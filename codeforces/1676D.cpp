#include<iostream>
#include<vector>

using namespace std;

int solve(int *arr, int n, int m, int row, int col) {
    int ans = 0;
    int board[n][m]; 
    for (int i=0; i<n; i++) {
        for (int j=0; j<m; j++) {
           board[i][j] = *((arr+m*i) + j);
        }
    }
    int r;
    int c;
    ans = ans+ board[row][col];
    //down right

    r=row;
    c=col;

    while(r >= 0 && r+1<n && c >=0 && c+1<m){
        r = r+1;
        c = c+1;
        ans = ans+board[r][c];
    }

    //down left
    r=row;
    c=col;

    while(r >= 0 && r+1<n && c-1 >=0 && c <m){
        r = r+1;
        c = c-1;
        ans = ans+board[r][c];
    }
    //up right
    r=row;
    c=col;

    while(r-1 >= 0 && r<n && c >=0 && c+1 <m){
        
        r = r-1;
        c = c+1;
        ans = ans+board[r][c];
    }
    //up left
    r=row;
    c=col;

    while(r-1 >= 0 && r<n && c-1 >=0 && c <m){
        
        r = r-1;
        c = c-1;
        ans = ans+board[r][c];
    }

    return ans;
}

int main() {
    vector<int> answer;
    int z;
    cin>>z;

    while(z--){
        int n, m;
        cin>>n>>m;
        int arr[n][m];
        for (int i=0; i<n; i++)
            for (int j=0; j<m; j++) {
                cin>>arr[i][j];
            }
        
        int soln[n][m];
        for (int i=0; i<n; i++)
            for (int j=0; j<m; j++)
                soln[i][j] = solve((int*)arr, n, m, i, j);

        int max = INT_MIN;
        for (int i=0; i<n; i++)
            for (int j=0; j<m; j++)
                if(soln[i][j] > max) max = soln[i][j];
        
        answer.push_back(max);
    }

    for(auto a: answer) {
        cout<<a<<endl;
    }
}