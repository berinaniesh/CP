#include<iostream>
#include<vector>
#include<algorithm>

int main() {
    std::string input;
    std::cin>>input;
    std::vector<char> nums;
    for(auto &ch: input) {
        if (ch == '+') continue;
        nums.push_back(ch);
    }
    sort(nums.begin(), nums.end());

    for (int i=0; i<nums.size(); i++) {
        std::cout<<nums[i];
        if (i!=nums.size()-1) {
            std::cout<<'+';
        }
    } 
}