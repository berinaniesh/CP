#include <iostream>
using namespace std;

int main() {
  int t;
  cin >> t;
  while (t--) {
    int n;
    string s;
    cin >> n;
    cin >> s;
    string ans = "";
    int count = 0;
    for(int i=0; i<n; i++) {
      if (s[i] == '0') {
	ans = ans + '+';
      } else {
	if (count%2 == 0) {
	  ans = ans+'+';
	} else {
	  ans = ans + '-';
	}
	count = count +1;
      }
    }
    string ans2;
    for(int i=1; i<n; i++) {
      ans2 = ans2+ans[i];
    }
    cout << ans2 << endl;
  }
}
