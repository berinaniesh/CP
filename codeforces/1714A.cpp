#include<iostream>
#include<vector>

using namespace std;

int time_diff(int h1, int m1, int h2, int m2) {
    if (h2 > h1) {
        if (m2 > m1) {
            return ((m2-m1) + 60 * (h2 - h1));
        } else if (m1>m2) {
            h2 = h2-1;
            m2 = m2 + 60;
            return ((m2-m1) + 60*(h2-h1));
        } else {
            return 60 * (h2-h1);
        } 
    } else if (h1 > h2) {
        h2 = h2 + 24;
        if (m2 > m1) {
            return ((m2-m1) + 60 * (h2 - h1));
        } else if (m1>m2) {
            h2 = h2-1;
            m2 = m2 + 60;
            return ((m2-m1) + 60*(h2-h1));
        } else {
            return 60 * (h2-h1);
        }
    } else {
        if (m2 > m1) {
            return (m2-m1);
        } else if (m1 > m2) {
            return (24*60 - (m1-m2));
        } else {
            return 0;
        }
    }
    return 8;
}

int main() {
    int n;
    cin>>n;

    while(n--) {
        int k;
        cin>>k;
        int arr[k+1][2];
        for(int i=0; i<=k; i++) {
            cin>>arr[i][0]>>arr[i][1];
        }
        int ans[k];
        for (int i=0; i<k; i++) {
            ans[i] = time_diff(arr[0][0], arr[0][1], arr[i+1][0], arr[i+1][1]);
        }
        int min=INT_MAX;
        for (int l=0; l<k; l++) {
            if (ans[l] < min) {
                min = ans[l];
            }
        }
        int hours = min/60;
        int minutes = min%60;
        cout << hours << " " << minutes << endl;
    }
    return 0;
}
