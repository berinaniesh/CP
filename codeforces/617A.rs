fn main() {
    let mut s: String = String::new();
    std::io::stdin().read_line(&mut s).unwrap();
    let n: usize = s.trim().parse::<usize>().unwrap();
    let ans: usize;
    if n%5 == 0 {ans = n/5} else {ans = n/5 + 1}
    println!("{}", ans);
}
