// Create your own template by modifying this file!
#include <bits/stdc++.h>
using namespace std;

#ifdef DEBUG
     #define debug(args...)            {dbg,args; cerr<<endl;}
#else
    #define debug(args...)              // Just strip off all debug tokens
#endif

struct debugger
{
    template<typename T> debugger& operator , (const T& v)
    {    
        cerr<<v<<" ";    
        return *this;    
    }
} dbg;

int main() 
{
    int arr[5][5];
    int r;
    int c;
    for(int i=0; i<5; i++)
        for(int j=0; j<5; j++){
            cin>>arr[i][j];
            if (arr[i][j] == 1){
                r = i;
                c = j;
            }
        }
    int ans = 0;
    while(r<2){
        r++;
        ans++;
    }
    while(r>2){
        r--;
        ans++;
    }
    while(c<2) {
        c++;
        ans++;
    }
    while(c>2) {
        c--;
        ans++;
    }

    cout<<ans<<endl;
}

