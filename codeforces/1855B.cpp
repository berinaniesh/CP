#include<iostream>
using namespace std;
 
void solve() {
  unsigned long long n;
  cin >> n;
  if (n==1) {
    cout << 1 << endl;
    return;
  }
  for (unsigned long long i=2; i<=n; i++) {
    if (n%i !=0) {
      cout << i-1 << endl;
      return;
    }
    if (i == n) {
      cout << i << endl;
      return;
    }
  }
}
 
int main() {
  unsigned long long t;
  cin >> t;
  while(t--) {
    solve();
  }
}
