
fn main() {
    let mut buffer = String::new();
    std::io::stdin().read_line(&mut buffer).unwrap();
    let n: isize = buffer.trim().parse().unwrap();
    if n%2 == 0 {
        println!("{}", n/2);
    } else {
        println!("{}", -(n+1)/2);
    }
}
