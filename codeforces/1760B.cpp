#include <algorithm>
#include <iostream>
using namespace std;

int main() {
  int t;
  cin >> t;
  while (t--) {
    int arr[3];
    cin >> arr[0] >> arr[1] >> arr[2];
    int n = sizeof(arr) / sizeof(arr[0]);
    sort(arr, arr + n);
    cout << arr[1] << endl;
  }
}
