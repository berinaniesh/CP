#include<iostream>

using namespace std;

bool check(int a, int b, int c) {
  if (a==b || b==c || a==c) return true;
  return false;
}

void solve() {
  int x1, y1, x2, y2, x3, y3;
  cin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3;
  bool x = check(x1, x2, x3);
  bool y = check(y1, y2, y3);
  if (x && y) {
    cout << "NO" << endl;
  } else {
    cout << "YES" << endl;
  }
}

int main() {
  int t;
  cin >> t;
  while(t--) solve();
}
