#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

bool is_even(int n) {
  if (n%2 ==0) return true;
  return false;
}

int convert(int n) {
  int tmp = n;
  int count= 0;
  if (is_even(n)) {
    while(tmp%2 == 0 && tmp !=0){
      tmp = tmp /2;
      count = count +1;
      if(tmp == 0) return INT32_MAX;
    }
    return count;
  } else {
    while(tmp%2 !=0) {
      tmp = tmp/2;
      count = count +1;
    }
    return count;
  
  }
  return 0;
}

int main() {
  int t;
  cin >> t;
  while(t--) {
    int n;
    cin >> n;
    vector<int> arr;
    while(n--) {
      int tmp;
      cin >> tmp;
      arr.push_back(tmp);
    }
    int sum = 0;
  for(auto k: arr) {
    sum += k;
  } 
  if (is_even(sum)) {
    cout << 0 << endl;
    continue;
  }
  vector<int> data_array;
  for (auto k: arr) {
    data_array.push_back(convert(k));
  }
  int min = *min_element(data_array.begin(), data_array.end());
  cout << min << endl;
  }
  return 0;
}
