fn main () {
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).unwrap();
    let trimmed = input.trim();
    let n = trimmed.parse::<u8>().unwrap();
    let mut vec = Vec::new();
    for _ in 0..n {
        let mut input = String::new();
        std::io::stdin().read_line(&mut input).unwrap();
        let to_push = String::from(input.trim_end());
        vec.push(to_push);
    }

    let mut ans:Vec<String> = Vec::new();

    for i in vec.iter() {
        if i.chars().count() <= 10 {
            ans.push(i.to_string());
            continue;
        }
        let mut ans_string = String::new();
        let ch = i.chars().next().unwrap();
        ans_string.push(ch);
        let length:u8 = (i.chars().count() - 2) as u8;
        let num_string = length.to_string();
        for j in num_string.chars() {
            ans_string.push(j);
        }
        ans_string.push(i.chars().last().unwrap());
        ans.push(ans_string);
    }

    for i in ans.iter() {
        println!("{}", i);
    }
}
